/**
 * \file
 * \author Mattia Basaglia
 * \copyright Copyright 2015-2018 Mattia Basaglia
 * \section License
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "settings.hpp"

namespace telegram {
namespace glax_weather {
    enum DayNight
    {
        Day = 'd',
        Night = 'n',
    };

    enum Condition
    {
        Clear = 1,
        FewClouds = 2,
        ScatteredClouds = 3,
        BrokenClouds = 4,
        ShowerRain = 9,
        Rain = 10,
        Thunderstorm = 11,
        Snow = 13,
        Fog = 50,
    };

    std::string glax_icon(const Settings& parsed);
} // namespace  glax_weather
} // namespace telegram
