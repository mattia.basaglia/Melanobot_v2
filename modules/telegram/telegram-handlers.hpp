/**
 * \file
 * \author Mattia Basaglia
 * \copyright Copyright 2015-2017 Mattia Basaglia
 * \section License
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOBOT_MODULES_TELEGRAM_HANDLERS_HPP
#define MELANOBOT_MODULES_TELEGRAM_HANDLERS_HPP

#include "core/handler/misc.hpp"
#include "inline.hpp"
#include "web/handler/web-api-concrete.hpp"
#include "glax_weather.hpp"

namespace telegram {

/**
 * \brief Sends a sticker
 */
class SendSticker : public core::Reply
{
public:
    SendSticker(const Settings& settings, MessageConsumer* parent)
        : Reply(settings, parent)
    {
        sticker_id = settings.get("reply", "");
        if ( sticker_id.empty() )
            throw melanobot::ConfigurationError("Missing sticker_id for SendSticker");
    }

    void on_handle(const network::Message& msg, string::FormattedString&& reply) const override
    {
        msg.destination->command({
            "sendSticker",
            {reply_channel(msg), sticker_id}
        });
    }

private:
    std::string sticker_id;
};

/**
 * \brief Handler searching a video on OpenWeatherMaps
 */
class DragonWeather : public web::OpenWeather
{
public:
    DragonWeather(const Settings& settings, MessageConsumer* parent)
        : OpenWeather(settings, parent)
    {
        reply = read_string(settings, "reply",
            "$(round $main.temp 1) C, $main.humidity% Humidity, $weather.0.description");
    }

protected:
    void json_success(const network::Message& msg, const Settings& parsed) override
    {
        std::string photo_url = glax_weather::glax_icon(parsed);
        msg.destination->command({
            "sendPhoto",
            {reply_channel(msg), photo_url, reply.replaced(parsed).encode(*msg.source->formatter())}
        });
    }
};

} // namespace telegram

#endif // MELANOBOT_MODULES_TELEGRAM_HANDLERS_HPP
