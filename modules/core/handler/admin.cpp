/**
 * \file
 * \brief This file defines handlers which allows admin to administrate the bot
 *
 * \author Mattia Basaglia
 * \copyright Copyright 2015-2018 Mattia Basaglia
 * \license
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "admin.hpp"
namespace core {


bool ConnectionStatus::on_handle(network::Message& msg)
{
    string::FormattedString quit_msg;
    auto& bot = melanobot::Melanobot::instance();
    for ( const auto& name : bot.connection_names() )
    {
        network::Connection* connection = bot.connection(name);
        if ( !connection )
        {
            reply_to(msg, not_found_message.replaced("name", name));
        }
        else
        {
            string::FormattedProperties prop = connection->pretty_properties();
            prop["name"] = name;
            prop["protocol"] = connection->protocol();

            color::Color12 status_color = color::blue;
            switch ( connection->status() )
            {
                case network::Connection::DISCONNECTED:
                    prop["status_name"] = "disconnected";
                    status_color = color::red;
                    break;
                case network::Connection::WAITING:
                    prop["status_name"] = "waiting";
                    status_color = color::yellow;
                    break;
                case network::Connection::CONNECTING:
                    prop["status_name"] = "connecting";
                    status_color = color::yellow;
                    break;
                case network::Connection::CHECKING:
                    prop["status_name"] = "checking";
                    status_color = color::yellow;
                    break;
                case network::Connection::CONNECTED:
                    prop["status_name"] = "connected";
                    status_color = color::green;
                    break;
            }
            prop["status_color"] = string::FormattedString() << status_color;
            reply_to(msg, message.replaced(prop));
        }
    }
    return true;
}

} // namespace core
